Feature: Amazon Product Search
  It is awesome!

  Background:
    Given I open "https://www.amazon.de"

  Scenario: As a user, I can search the products on amazon.de in order to order my favorite product.
    If this does not work, amazon will lose 900% share value.
     When I enter "Haarbürste" into the product search
      And I press the product search button
     Then I should see the product "Haarbürste" in the result list