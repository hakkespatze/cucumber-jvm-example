package de.florianreinhard;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Manage starting and stopping browsers
 */
public class BrowserHooks {

    //TODO relocate accordingly, once Page Object Pattern is implemented
    public static WebDriver driver;

    @Before
    public void beforeScenario() throws MalformedURLException {
        DesiredCapabilities caps = DesiredCapabilities.firefox();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), caps);
    }

    @After
    public void afterScenario(){
        driver.quit();
    }
}
