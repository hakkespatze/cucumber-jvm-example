package de.florianreinhard;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

// For more information about categories, see https://community.oracle.com/blogs/johnsmart/2010/04/25/grouping-tests-using-junit-categories-0
@Category(IntegrationTestsCategory.class)
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:report.html"}, features = "src/test/resources/features", glue = "de.florianreinhard")
public class RunCukesIntegrationTests {}


