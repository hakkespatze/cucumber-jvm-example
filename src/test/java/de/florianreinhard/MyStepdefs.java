package de.florianreinhard;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by florian on 21.08.18.
 */
public class MyStepdefs {

    @Given("I open \"(.*)\"")
    public void i_open(String url) {
        BrowserHooks.driver.get(url);
    }

    @When("I enter \"(.*)\" into the product search")
    public void i_enter_into_the_product_search(String productName) {
        BrowserHooks.driver.findElement(By.cssSelector("#twotabsearchtextbox")).sendKeys(productName);
    }

    @When("I press the product search button")
    public void i_press_the_product_search_button() {
        BrowserHooks.driver.findElement(By.cssSelector(".nav-search-submit input")).click();
    }

    @Then("I should see the product \"(.*)\" in the result list")
    public void i_should_see_the_product_in_the_result_list(String productName) {
        List<WebElement> results =  BrowserHooks.driver.findElements(By.cssSelector("li[id^='result']"));
        Assert.assertTrue(results.size() > 1);
    }

}
